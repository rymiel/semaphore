// Shutter positions and sizes for each station
const shutterSpacing = [-31, -21, -11, 11, 21, 31, 45];
const shutterScaling = [8, 8, 8, -8, -8, -8, -10];

// Station variables
var stations = [];
var stationUnderCursor = null;
var selectionLocked = null;
var ghostStation = null;

// List of all modes
const modeTemplate = [
  ["observe", [200, 210, 230], "O"],
  ["add",     [200, 240, 210], "A", {onSwitchTo: newGhostStation}],
  ["connect", [220, 220, 180], "C", {halo: {
    hoverWhenAlsoSelection: [255, 127, 90, 150],
    hoverAfterSelect: [255, 140],
    select: [255, 255, 55, 140],
  }}],
  ["send",    [230, 170, 220], "S", {halo: {
    hoverAfterSelect: [255, 140],
    select: [240, 110, 230, 220],
  }}],
  ["delete",  [240, 210, 205], "D", {halo: {
    hover: [250, 100, 100, 140],
  }}],
];

// Framerate tracking variables
var frameSmoothing = 0;
var frameSmoothThreshold = 15;
var frameTotal = 0;
var frameMinRecord = NaN;
var smoothFrameRate = 0;
var smoothFrameMin = NaN;

// Keyboard tracking variables
var keyBuffer = "";
var canSwitch = true;

// Station naming scheme
// TODO: refactor
var stationCounter = 101;

// Perspective- and camera-based variables
var camera;
var cursorX;
var cursorY;

function setup() {
  createCanvas(800, 800);
  noCursor();
  camera = new Camera(0, 0, 1);
  mode.setup(modeTemplate);
}

function draw() {
  // push state for camera translation. draws need to be made before 
  // the camera is moved and scaled should be placed above here
  push(); 

  logger.logCount = 0; // reset log drawer position to the bottom of the screen

  // draw info determined by the mode, such as the background and description
  // in the lower left
  mode.draw();

  // Smooth FPS~
  // Logs "Average FPS / Minimum FPS"
  logger.frameLog("FPS: " + smoothFrameRate + "/" + smoothFrameMin, logger.UI);
  if (frameSmoothing === frameSmoothThreshold) {
    frameSmoothing = 0;
    smoothFrameRate = parseInt(frameTotal / frameSmoothThreshold);
    frameTotal = 0;
    smoothFrameMin = parseInt(frameMinRecord);
    frameMinRecord = NaN;
  } else {
    let currentFPS = frameRate();
    frameTotal += currentFPS;
    if (frameMinRecord > currentFPS || isNaN(frameMinRecord)) {
      frameMinRecord = currentFPS;
    }
    frameSmoothing += 1;
  }
  logger.frameLog("Zoom level: " + parseInt(camera.scale * 100) + "% .: " + parseInt(camera.scaleAcceleration * 1000) / 10, logger.UI);

  // If in a position to manually send a message, draw the actual send dialog
  if (mode.is("send") && selectionLocked) logger.frameLog("Send: " + keyBuffer, logger.UI);

  // Translate camera
  camera.position();

  push();
  let weight = 1;
  let color = 40;
  let a = 130;
  let originX = camera.x - (width / 2) * (1 / camera.scale);
  let originY = camera.y - (height / 2) * (1 / camera.scale);
  let endX = originX + width * (1 / camera.scale);
  let endY = originY + height * (1 / camera.scale);
  for (let j = 2; j <= 3; j += 1) {
    let i = Math.pow(12, j);
    strokeWeight(weight);
    stroke(color, a);
    let latticeX = -originX % i;
    let latticeY = -originY % i;
    while (latticeX + originX < endX) {
      line(latticeX + originX, originY, latticeX + originX, endY);
      latticeX += i;
    }
    while (latticeY + originY < endY) {
      line(originX, latticeY + originY, endX, latticeY + originY);
      latticeY += i;
    }
    weight *= 2;
    color /= 2;
    a += 50;
  }
  pop();

  // Correctly position "ghost station" which shows a preview of next station in add mode.
  if (mode.is("add")) {
    ghostStation.x = cursorX;
    ghostStation.y = cursorY;
    ghostStation.draw();
  }

  // Calculate if there's a station close enough to the cursor to be "under it"
  stations.forEach((i) => i.distanceToMouse = dist(i.x, i.y, cursorX, cursorY));
  let stationsByDistance = [...stations];
  stationsByDistance.sort(function(a, b) {
    return a.distanceToMouse - b.distanceToMouse;
  });
  let closestStation = stationsByDistance[0] || NaN;
  if (closestStation.distanceToMouse < 30) {
    stationUnderCursor = closestStation;
  } else stationUnderCursor = null;

  let selectionDiffers = stationUnderCursor !== selectionLocked;

  // Draw transparent circles around some stations to better highlight selection, determined by the current mode.
  mode.drawHalos();

  // Connecting mode in-progress arrows which aren't handled by the halo system
  if (selectionLocked && mode.is("connect")) {
    push();
    strokeWeight(5);
    let x1, y1;
    if (stationUnderCursor && selectionDiffers) {
      x1 = stationUnderCursor.x;
      y1 = stationUnderCursor.y;
      stroke("orange");
    } else {
      x1 = cursorX;
      y1 = cursorY;
      stroke("yellow");
    }
    drawArrow(selectionLocked.x, selectionLocked.y, x1, y1);
    pop();
  }

  // Draw all arrows and stations.
  stations.forEach((i) => i.drawAdjacent());
  stations.forEach((i) => i.draw());

  // Draw cursor dot that doesn't scale but translates. Placed here so it goes above most things.
  push();
  strokeWeight(1 * (1 / camera.scale));
  ellipse(cursorX, cursorY, 10 * (1 / camera.scale));
  pop();

  // Unshift camera. Log drawing is done after this as to have them not move or scale with the rest of the sketch.
  pop();

  logger.logFrameMessages();
  logger.logFlashMessages();
}

class Station {
  constructor(x, y, scale, shutters, name) {
    this.x = x;
    this.y = y;
    this.scale = scale;
    this.distanceToMouse = NaN;
    this.name = name;

    if (shutters === undefined) {
      shutters = [0, 0, 0, 0, 0, 0, 0];
    }
    this.shutters = shutters;
    this.shuttersWas = [0, 0, 0, 0, 0, 0, 0];
    this.shuttersR = [0, 0, 0, 0, 0, 0, 0];

    this.shutterFlag = 0;
    this.shutterArgument = 0;
    this.shutterAcceleration = 0.01;

    this.adjacent = [];
    this.inbound = null;
    this.ignoring = [];

    this.state = "idle";
    this.waitingForConfirmation = false;
    this.delay = 0;
    this.buffer = [];

    this.inTransmission = "000";
  }

  draw() {
    let moved = this.checkMoved();
    this.step();
    if (moved === true) this.complete();

    push();
    textAlign(CENTER);
    text("station " + this.name, this.x, this.y - (50 * this.scale) - 12);
    if (this.inTransmission !== "000") text(this.state + " - " + this
      .inTransmission, this.x, this.y - (50 * this.scale));
    else text(this.state, this.x, this.y - (50 * this.scale));
    pop();

    push();
    fill(0);
    strokeWeight(0);
    for (let i = 0; i <= 7; i++) {
      this.drawShutter(i);
    }
    pop();

    push();
    stroke("brown");
    strokeWeight(3 * this.scale);
    line(this.x, this.y, this.x, this.y - (25 * this.scale));
    strokeWeight(5 * this.scale);
    line(this.x - (30 * this.scale), this.y - (25 * this.scale), this.x + (
      44 * this.scale), this.y - (25 * this.scale));
    pop();
  }

  drawShutter(num) {
    rect(this.x + (shutterSpacing[num] * this.scale), this.y - (25 * this
      .scale), shutterScaling[num] * this.scale, 20 * this.scale - (this
      .shuttersR[num] * 20 * this.scale));
  }

  drawAdjacent() {
    push();
    if (mode.is("connect")) strokeWeight(5);
    else strokeWeight(2);
    stroke("red");
    this.adjacent.forEach((i) => drawArrow(this.x, this.y, i.x, i.y));
    pop();
  }

  checkMoved() {
    // text(this.shutterFlag, this.x, this.y-60)
    if (arraysEqual(this.shutters, this.shuttersWas))
      return false; // No movement needed
    // Shutters dropped, ready to be put in position
    if (this.shutterFlag === 0 && arraysEqual(this.shuttersR, [0, 0, 0, 0, 0,
      0, 0
    ])) {
      this.shutterFlag = 1;
      this.shutterArgument = 20;
      return;
    }
    // Shutter move delay
    if (this.shutterFlag === 1 && this.shutterArgument > 0) {
      this.shutterArgument -= 1;
      return;
    }
    // Shutter delay over
    if (this.shutterFlag === 1 && this.shutterArgument === 0) {
      this.shutterFlag = 2;
      return;
    }
    // Shutters in desired position
    if (arraysEqual(this.shutters, this.shuttersR)) {
      this.shuttersWas = [...this.shutters];
      this.shutterFlag = 0;
      this.shutterArgument = 0;
      return true;
    }
    // Shutters dropping
    if (this.shutterFlag === 0) {
      this.shutterArgument += this.shutterAcceleration;
      for (let i = 0; i <= 6; i++) {
        if (this.shuttersR[i] > 0) this.shuttersR[i] -= this.shutterArgument;
        else this.shuttersR[i] = 0;
      }
      return;
    }
    // Shutters moving towards desired position
    for (let i = 0; i <= 6; i++) {
      if (this.shuttersR[i] != this.shutters[i]) {
        let diff = this.shutters[i];
        diff /= 50;
        this.shuttersR[i] += diff + this.shutterArgument;
        if (diff > 0 && this.shuttersR[i] >= this.shutters[i]) this.shuttersR[
          i] = this.shutters[i];
        if (diff < 0 && this.shuttersR[i] <= this.shutters[i]) this.shuttersR[
          i] = this.shutters[i];
        this.shutterArgument += (this.shutterAcceleration / 6);
      }
    }
  }

  step() {
    if (this.state === "reacting" && this.delay > 0) this.delay--;
    else if (this.state === "reacting" && this.delay == 0) this.react([...this
      .inbound.shutters
    ]);
    else if (this.state === "confirming" && this.delay > 0) this.delay--;
    else if (this.state === "confirming" && this.delay == 0) this.continue();
  }

  react(sign) {
    if (!this._react(sign)) {
      this.discard();
    }
  }

  _react(sign) {
    let s = shuttersToString(sign).padStart(4, " ").split("");
    if (s[1] === "5" && s[2] === "7") {
      s[2] = "8";
      logger.flash("centerbound stations of " + this.inbound.name + " are " + this.inbound.centerbound.map((i) => i.name));
      if (s[3] === "1" && !this.inbound.centerbound.includes(this)) return;
    }
    this.broadcast(stringToShutters(s.join("")));
    return true;
  }

  broadcast(sign, convert = false) {
    if (!sign) {
      this.goIdle();
    } else if (this.isReady()) {
      this.state = "relaying";
      this.setShutters(sign, convert);
    }
  }

  requestAttention(requester) {
    if (requester.inbound !== this && this.canContinue()) {
      this.delay = 30 + getRandomInt(100);
      this.state = "reacting";
      this.inbound = requester;
    } else if (requester.inbound !== this) {
      this.waitingForConfirmation = true;
      if (this.getOutbound().length === 0) this.continue();
    } else {
      if (this.state === "transmitted") {
        if (this.getOutbound().filter((i) => i.state === "relaying" || i
          .state === "reacting").length > 0) return;
        this.state = "confirming";
        this.delay = 30 + getRandomInt(50);
      }
    }
  }

  complete() {
    if (this.state === "relaying") {
      this.state = "transmitted";
      this.getNeighbours().forEach((i) => i.requestAttention(this));
    }
  }

  continue () {
    if (this.waitingForConfirmation) {
      this.waitingForConfirmation = false;
      this.state = "reacting";
      this.delay = 10 + getRandomInt(15);
    } else if (this.inbound === null) {
      logger.flash("centerbound stations of " + this.name + " are " + this.centerbound.map((i) => i.name));
      this.broadcast(this.buffer.shift(), true);
    } else if (this.inbound.state === "idle") {
      this.goIdle();
    }
  }

  ignore(station) {
    logger.flash("told to ignore " + station.name);
    if (this.ignoring.includes(station)) return;
    this.ignoring.push(station);
    station.goIdle();
    if (this.ignoring.length === this.getNeighbours().length) this.goIdle();
  }

  goIdle() {
    this.state = "idle";
    this.setShutters([0, 0, 0, 0, 0, 0, 0]);
    this.inbound = null;
    this.ignoring = [];
    this.getOutbound().forEach(function(i) {
      if (i.state === "transmitted" && i.getOutbound().length === 0) i
        .goIdle();
    });
  }

  discard() {
    this.state = "discarded";
    this.setShutters([0, 0, 0, 0, 0, 0, 0]);
    this.inbound = null;
    let me = this;
    this.getOutbound().forEach(function(i) {
      if (i.state === "transmitted") i.ignore(me);
    });
  }

  setShutters(toSet, convert = false) {
    if (convert) toSet = stringToShutters(toSet);
    this.shutters = toSet;
    this.inTransmission = shuttersToString(toSet);
  }

  isReady() {
    return this.shutterFlag === 0 && this.shutterArgument === 0;
  }

  canContinue() {
    return this.state === "confirmed" || this.state === "idle";
  }

  get edgebound() {
    return [...this.adjacent];
  }

  get centerbound() {
    let valid = [];
    for (let i of stations) {
      if (i.adjacent.includes(this)) valid.push(i);
    }
    return valid;
  }

  getNeighbours(autoIgnore = true) {
    if (autoIgnore) return [...this.edgebound, ...this.centerbound].filter((i) => !this.ignoring.includes(i));
    else return [...this.edgebound, ...this.centerbound];
  }

  getOutbound(autoIgnore = true) {
    return this.getNeighbours(autoIgnore).filter((i) => i !== this.inbound);
  }
}

class Camera {
  constructor(x, y, scale) {
    this.x = x;
    this.y = y;
    this.dx = 0;
    this.dy = 0;
    this.dx_prev = 0;
    this.dy_prev = 0;
    this.ox = 0;
    this.oy = 0;
    this.scale = scale;
    this.dscale = 0;
    this.scaleAcceleration = 0;

    this.speed = 20;
    this.friction = 0.25;
  }

  setMomentumX(dx) {
    this.dx += dx * 2 * (1 / (this.scale + 1));
  }

  setMomentumY(dy) {
    this.dy += dy * 2 * (1 / (this.scale + 1));
  }

  resetMomentum() {
    this.dx = 0;
    this.dy = 0;
    this.dscale = 0;
  }

  position() {
    this.resetMomentum();
    if (keyIsDown(38)) this.setMomentumY(-this.speed);
    if (keyIsDown(40)) this.setMomentumY(this.speed);
    if (keyIsDown(37)) this.setMomentumX(-this.speed);
    if (keyIsDown(39)) this.setMomentumX(this.speed);
    if (keyIsDown(81)) this.dscale += (0.01 + this.scaleAcceleration) * this.scale;
    if (keyIsDown(69)) this.dscale += -(0.01 + this.scaleAcceleration) * this.scale;

    if (keyIsDown(81) || keyIsDown(69)) this.scaleAcceleration += 0.001;
    if (this.dscale === 0) this.scaleAcceleration = 0;
    this.scale += this.dscale;

    if (this.scale < 0.25) {
      this.scale = 0.25;
      this.scaleAcceleration = 0;
    } else if (this.scale > 4) {
      this.scale = 4;
      this.scaleAcceleration = 0;
    }

    if (Math.abs(this.dx) < Math.abs(this.dx_prev)) {
      if (Math.abs(this.dx_prev) < 0.1) this.dx = 0;
      else this.dx = this.dx_prev / (this.friction + 1);
    }
    if (Math.abs(this.dy) < Math.abs(this.dy_prev)) {
      if (Math.abs(this.dy_prev) < 0.1) this.dy = 0;
      else this.dy = this.dy_prev / (this.friction + 1);
    }

    logger.frameLog(this.dx + ", " + this.dy);
    this.x += this.dx;
    this.y += this.dy;
    this.dx_prev = this.dx;
    this.dy_prev = this.dy;

    cursorX = this.x + (mouseX - width / 2) / this.scale;
    cursorY = this.y + (mouseY - height / 2) / this.scale;
    this.ox = -this.x + width / 2 / this.scale;
    this.oy = -this.y + height / 2 / this.scale;

    scale(this.scale);
    translate(this.ox, this.oy);
  }
}

function drawArrow(xa, ya, xb, yb) {
  push();
  let x2, y2, x3, y3;
  x2 = xb - (xb - xa) / 2;
  y2 = yb - (yb - ya) / 2;
  x3 = x2 - xa;
  y3 = y2 - ya;
  line(xa, ya, xb, yb);
  translate(x2, y2);
  rotate(createVector(x3, y3).heading());
  line(0, 0, -10, -10);
  line(0, 0, -10, 10);
  pop();
}

function newGhostStation() {
  ghostStation = null;
  ghostStation = new Station(cursorX, cursorY, 1, [0, 0, 0, 0, 0, 0, 0], stationCounter.toString() + " (next)");
  ghostStation.state = "click to place";
}

function mouseClicked() {
  if (mode.is("add") && mouseX <= width && mouseY <= height) {
    stations.push(new Station(cursorX, cursorY, 1, [0, 0, 0, 0, 0, 0, 0],
      stationCounter.toString()));
    stationCounter++;
    if (stationCounter.toString()[2] === "9") stationCounter++;
    newGhostStation();
    return false;
  }
}

function mousePressed() {
  if (stationUnderCursor && mode.is("connect", "send")) {
    selectionLocked = stationUnderCursor;
    return false;
  } else if (stationUnderCursor && mode.is("delete")) {
    logger.flash("deleting " + stationUnderCursor.name);
    stationUnderCursor.centerbound.forEach((i) => i.adjacent = i.adjacent.filter((s) => s !== stationUnderCursor));
    stations = stations.filter((i) => i !== stationUnderCursor);
    return false;
  }
}

function mouseReleased() {
  if (selectionLocked && mode.is("connect")) { // TODO: put connection remove logic here
    if (stationUnderCursor && selectionLocked !== stationUnderCursor && !
    selectionLocked.adjacent.includes(stationUnderCursor) && !
    stationUnderCursor.adjacent.includes(selectionLocked)) {
      selectionLocked.adjacent.push(stationUnderCursor);
      logger.flash("connection made " + selectionLocked.name + " -> " +
        stationUnderCursor.name);
    }
    selectionLocked = null;
  }
  return false;
}

function keyPressed() {
  if (keyCode === 82) {
    camera.scale = 1;
    camera.scaleAcceleration = 0;
    camera.x = 0;
    camera.y = 0;
  } else if (keyCode === 16) {
    canSwitch = false;
    camera.speed = 8;
    camera.friction = 0.6;
  } else if (keyCode === 8) {
    if (keyBuffer) {
      keyBuffer = keyBuffer.slice(0, keyBuffer.length - 1);
    } else selectionLocked = null;
    return false;
  } else if (keyCode === 13) {
    selectionLocked.buffer = keyBuffer.split(" ");
    selectionLocked.continue();
    keyBuffer = "";
    return false;
  }
  if (canSwitch) {
    mode.switchMode(keyCode);
  }
}

function keyReleased() {
  if (keyCode === 16) {
    canSwitch = true;
    camera.speed = 20;
    camera.friction = 0.25;
  }
}

function keyTyped() {
  if (["1", "2", "3", "4", "5", "6", "7", "8", "0", "A", "O", " "].includes(
    key)) {
    keyBuffer += key;
    return false;
  }
}

function stringToShutters(s) {
  let d = digitToShutter;
  s = s.padStart(3, "0").padStart(4, " ");
  return [...d(s[1]), ...d(s[2]), ...d(s[3]), ...d(s[0])];
}

function digitToShutter(val) {
  return {
    "0": [0, 0],
    "1": [0, 1],
    "2": [0, 2],
    "3": [1, 0],
    "4": [1, 1],
    "5": [1, 2],
    "6": [2, 0],
    "7": [2, 1],
    "8": [2, 2],
    "A": [2],
    "O": [1],
    " ": [0]
  } [val];
}

function shutterToDigit(first, second = 3) {
  if (second === 3) {
    second = first;
    first = 3;
  }
  return [
    ["0", "1", "2"],
    ["3", "4", "5"],
    ["6", "7", "8"],
    ["", "O", "A"]
  ][first][second];
}

function shuttersToString(s) {
  let d = shutterToDigit;
  return d(s[6]) + d(s[0], s[1]) + d(s[2], s[3]) + d(s[4], s[5]);
}

function arraysEqual(a1, a2) {
  /* WARNING: arrays must not contain {objects} or behavior may be undefined */
  return JSON.stringify(a1) == JSON.stringify(a2);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

const logger = {
  logFrame: [],
  logHistory: [],
  logCount: 0,
  LOG: 0,
  FLASH: 1,
  UI: 2,
  _frameLog: function(t, messageType = logger.LOG, opacity = 255) {
    push();
    strokeWeight(2);
    switch (messageType) {
    case this.LOG:
      stroke(200, opacity);
      fill(100, opacity);
      break;
    case this.FLASH:
      stroke(255, 50, 50, opacity);
      fill(255, opacity);
      break;
    case this.UI:
      stroke(255, opacity);
      fill(0, opacity);
      break;
    }
    text(t, 20, height - 20 - (12 * logger.logCount));
    logger.logCount++;
    pop();
  },

  flash: function(t, duration = 240) {
    logger.logHistory.push([t, duration]);
  },
  
  frameLog: function(...t) {
    logger.logFrame.push(t);
  },

  logFlashMessages: function() {
    for (let s of logger.logHistory) {
      logger._frameLog(s[0], logger.FLASH, min(255, s[1] * (255 / 30)));
      s[1]--;
    }
    logger.logHistory = logger.logHistory.filter((i) => i[1] > 0);
  },
  
  logFrameMessages: function () {
    for (let s of logger.logFrame) {
      logger._frameLog(...s);
    }
    logger.logFrame = [];
  }
};

const mode = {
  Mode: class Mode {
    /**
     * @param  {string} name
     * @param  {number[]} color
     * @param  {number} keyCode
     */
    constructor(name, color, keyCode, additional = {}) {
      this.name = name;
      this.color = color;
      this.keyCode = keyCode.charCodeAt(0);
      this.id = mode.counter;
      this.onSwitchTo = additional.onSwitchTo || (() => null);
      this.halo = additional.halo || {};
    }

    switchTo() {
      mode.currentIndex = this.id;
      this.onSwitchTo();
    }

    drawHalo(relevantStation, haloKey) {
      logger.frameLog(haloKey + " on " + relevantStation.name);
      if (this.halo[haloKey] !== undefined) {
        push();
        strokeWeight(0);
        fill(...this.halo[haloKey]);
        ellipse(relevantStation.x, relevantStation.y, 60);
        pop();
      }
    }
  },
  modes: [],
  currentIndex: 0,
  get current() { return mode.modes[mode.currentIndex]; },
  counter: 0,
  switchMode(possibleKeyCode) {
    if (this.modes.forEach((i) => { if (i.keyCode === possibleKeyCode) { i.switchTo(); return true; }})) {
      selectionLocked = null;
      keyBuffer = "";
    }
  },
  draw() {
    background(this.current.color);
    logger.frameLog("currently in " + this.current.name + " mode", logger.UI);
  },
  drawHalos() {
    let t = this.current;
    let h = t.halo;
    if (Object.keys(h).length > 0) {
      let selectionDiffers = stationUnderCursor !== selectionLocked;
      if (h.hoverWhenAlsoSelection && stationUnderCursor && selectionLocked && selectionDiffers) {
        t.drawHalo(stationUnderCursor, "hoverWhenAlsoSelection");
      } else if (h.hoverAfterSelect && stationUnderCursor && selectionDiffers) {
        t.drawHalo(stationUnderCursor, "hoverAfterSelect");
      } else if (h.hover && stationUnderCursor) {
        t.drawHalo(stationUnderCursor, "hover");
      }
      if (h.selectAfterHover && selectionLocked && selectionDiffers) {
        t.drawHalo(selectionLocked, "selectAfterHover");
      } else if (h.select && selectionLocked) {
        t.drawHalo(selectionLocked, "select");
      }
    }
  },
  is: (...checkModes) => checkModes.some((i) => i === mode.current.name),
  /**
   * @param  {any[]} allModes
   */
  setup(allModes) {
    for (let i of allModes) {
      this.modes.push(new this.Mode(...i));
      this.counter++;
    }
  }
};
